import 'package:flutter/material.dart';

class VideoPlayer extends StatefulWidget {
  const VideoPlayer({Key? key}) : super(key: key);

  @override
  State<VideoPlayer> createState() => _VideoPlayerState();
}

class _VideoPlayerState extends State<VideoPlayer> {
  var isShow = false;

  @override
  Widget build(BuildContext context) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return OrientationBuilder(builder: (context, orientation) {
      return isPortrait
          ? Column(
              children: [
                AspectRatio(
                  aspectRatio: 16 / 9,
                  child: Container(
                    color: Colors.black,
                    child: const Center(
                      child: Icon(
                        Icons.play_circle,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                const VideoDescription(),
              ],
            )
          : Stack(alignment: AlignmentDirectional.center, children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AspectRatio(
                    aspectRatio: 1 / 6,
                    child: SizedBox(),
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.black,
                      child: const Center(
                        child: Icon(
                          Icons.play_circle,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  AspectRatio(
                    aspectRatio: 1 / 6,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FloatingActionButton.small(
                            onPressed: () {
                              setState(() {
                                isShow = !isShow;
                              });
                            },
                            child: Icon(Icons.info_outline),
                          ),
                        ),
                        // const VideoDescription(),
                      ],
                    ),
                  )
                ],
              ),
              Visibility(
                  visible: isShow,
                  child: ClipRRect(
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.2,
                      height: MediaQuery.of(context).size.height * 0.4,
                      color: Colors.grey.shade300.withOpacity(0.8),
                      child: Center(child: const VideoDescription()),
                    ),
                    borderRadius: BorderRadius.circular(20),
                  )),
            ]);
    });
  }
}

class VideoDescription extends StatelessWidget {
  const VideoDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: const [
          Text(
            'Video Title',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 24),
          ),
          Text(
            'Video description',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 18),
          ),
          Text(
            'Likes: 34',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }
}
